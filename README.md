# Print to PDF

[electron-quick-start](https://electronjs.org/docs/tutorial/quick-start)をベースに、画面のPDF出力機能を追加しています。

### 使い方

**ソースコードのトランスパイル**

`$ npm run build:webpack`

**Electronアプリケーションの起動**

`$ npm start`

**バイナリのビルド**

`$ node build-win.js`

`$ node build-mac.js`


### 付帯して導入してあるもの
- Webpack + babelのビルド環境（レンダリングプロセスのみ）
- Windows向けビルドスクリプト(`build-win.js`)
- OS X向けビルドスクリプト(`build-mac.js`)

※windowsビルドは、windows環境でしかできない模様

※ビルド後に、下記資材を`Electron.app/Contents/Resources/app/`配下（Mac）、もしくは`electron/resources/app`(windows,Linux)配下に配置する必要があります。

```
package.json
main.js
index.html
style.css
preload.js
dist/main.js
```


## PDF出力に関するコードについて

### electronのメインプロセス（main.js）の編集箇所
contextBridge経由で`printToPdf`イベントを受け取り、Electronの[webContents API](https://www.electronjs.org/docs/api/web-contents#contentsprinttopdfoptions)を利用してPDF出力を行います。

```main.js

const {app, BrowserWindow, ipcMain, dialog } = require('electron')

...

ipcMain.handle('printToPdf', (event) => {
  const win = BrowserWindow.fromWebContents(event.sender);

  const option = {
    properties: ['openFile'],
    filters: [
      {
        name: 'export',
        extensions: ['pdf']
      }
    ]
  };

  // ファイル保存ダイアログを表示
  dialog.showSaveDialog(win, option).then(result => {
      console.log(result);
      if (!result.canceled) {
        // ファイルを保存
        win.webContents.printToPDF({}).then(data => {
          fs.writeFile(result.filePath, data, err => {
            if (err) return console.log(err.message);

          })
        })
      }
    }
  )
});
```

### プリロード（preload.js）の編集箇所
[contextBridge](https://www.electronjs.org/docs/api/context-bridge#contextbridge)を利用して、
レンダリングプロセスからのイベントを受け取る`myapi`を作成し、メインプロセスに渡します。

```
const { contextBridge, ipcRenderer } = require('electron')

contextBridge.exposeInMainWorld('myapi', {
    printToPdf: async (data) => await ipcRenderer.invoke('printToPdf')
  }
)
```

### レンダリングプロセス(/src)の編集箇所
canvas描画のテストを行うための[Chart.js](https://www.chartjs.org/)の導入と、
`myapi`のイベントを呼び出すクリックイベントを登録しています。
```
// Chart.jsを表示してみる
import Chart from 'chart.js/auto';
let ctx = document.getElementById('myChart').getContext('2d');
let myChart = new Chart(ctx, {
  ...
});


let printButton = document.getElementById('printButton');
printButton.addEventListener('click', () => {
  window.myapi.printToPdf().then( () => {
    console.log('print complete!');
  });
});
```



# electron-quick-start

**Clone and run for a quick way to see Electron in action.**

This is a minimal Electron application based on the [Quick Start Guide](https://electronjs.org/docs/tutorial/quick-start) within the Electron documentation.

**Use this app along with the [Electron API Demos](https://electronjs.org/#get-started) app for API code examples to help you get started.**

A basic Electron application needs just these files:

- `package.json` - Points to the app's main file and lists its details and dependencies.
- `main.js` - Starts the app and creates a browser window to render HTML. This is the app's **main process**.
- `index.html` - A web page to render. This is the app's **renderer process**.

You can learn more about each of these components within the [Quick Start Guide](https://electronjs.org/docs/tutorial/quick-start).

## To Use

To clone and run this repository you'll need [Git](https://git-scm.com) and [Node.js](https://nodejs.org/en/download/) (which comes with [npm](http://npmjs.com)) installed on your computer. From your command line:

```bash
# Clone this repository
git clone https://github.com/electron/electron-quick-start
# Go into the repository
cd electron-quick-start
# Install dependencies
npm install
# Run the app
npm start
```

Note: If you're using Linux Bash for Windows, [see this guide](https://www.howtogeek.com/261575/how-to-run-graphical-linux-desktop-applications-from-windows-10s-bash-shell/) or use `node` from the command prompt.

## Resources for Learning Electron

- [electronjs.org/docs](https://electronjs.org/docs) - all of Electron's documentation
- [electronjs.org/community#boilerplates](https://electronjs.org/community#boilerplates) - sample starter apps created by the community
- [electron/electron-quick-start](https://github.com/electron/electron-quick-start) - a very basic starter Electron app
- [electron/simple-samples](https://github.com/electron/simple-samples) - small applications with ideas for taking them further
- [electron/electron-api-demos](https://github.com/electron/electron-api-demos) - an Electron app that teaches you how to use Electron
- [hokein/electron-sample-apps](https://github.com/hokein/electron-sample-apps) - small demo apps for the various Electron APIs

## License

[CC0 1.0 (Public Domain)](LICENSE.md)
